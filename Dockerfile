FROM openjdk:8-jdk-alpine
EXPOSE 8021
ADD target/*.jar app.jar
ENTRYPOINT ["sh", "-c", "java -Dspring.config.location=classpath:/application.properties,/opt/demo-rest/config/application.properties -jar /app.jar"]