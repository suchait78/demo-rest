package com.restdemo.demorest.web;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.restdemo.demorest.constant.Constants;
import com.restdemo.demorest.jersey.JerseyClientConfig;

@Service
public class JmsService {

	public static final Logger LOG = LoggerFactory.getLogger(JmsService.class);
	
	@Autowired
	private JerseyClientConfig jerseyConfig;

	/*
	@Autowired
	@Qualifier("jmsDiscovery")
	private URI jmsServiceApplicationURL;
	
	public Response callJmsService(String componentHealthUrl) {

		Client client = ClientBuilder.newClient(jerseyConfig.getClientConfig());

		WebTarget webTarget = client.target(componentHealthUrl);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header(Constants.CONTENT_TYPE, MediaType.APPLICATION_JSON);

		return invocationBuilder.get();
	}
	
	public String buildURL() {
		
		LOG.info("Service instance picked : " + this.jmsServiceApplicationURL);
		
		return  this.jmsServiceApplicationURL.toString() + "/hello-world";
	}
	
	*/
}

