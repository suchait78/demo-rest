package com.restdemo.demorest.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.RestController;

@RestController
@Path("/demo")
public class DemoController {

	@Path("/string")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public String getDemoString() {
		
		return "demo string";
	}
	
}
