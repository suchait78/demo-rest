package com.restdemo.demorest.controller;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restdemo.demorest.web.JmsService;

@RestController
public class TestController {

	@Value("${any.value}")
	private String anyValue;

	@Value("${any.value.second}")
	private String secondAnyValue;
	
	/*
	@Autowired
	private JmsService jmsService;
	*/
	@GetMapping("/get/teststring")
	public String processController() {

		return "Hello World!";
	}

	@Autowired
	ConfigurableEnvironment env;
	
	@GetMapping("/get/value")
	public String processValue() {
		return anyValue + " " + secondAnyValue + " " + env.getProperty("any.value");
	}

    /*
	@GetMapping("/get/jms/data")
	public String callJmsService() {
		
		String data = null;
		
		String url = jmsService.buildURL();
		Response response = jmsService.callJmsService(url);
		
		if(response.getStatus() == Response.Status.OK.getStatusCode()) {
			data = response.readEntity(String.class);
		}
		
		return data;
	}
	*/
	
	
}
