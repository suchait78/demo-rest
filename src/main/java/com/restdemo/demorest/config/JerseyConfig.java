package com.restdemo.demorest.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.restdemo.demorest.controller.JerseyTestController;
import com.restdemo.demorest.filter.RequestContextFilter;

@ApplicationPath("/demo-rest")
@Configuration
public class JerseyConfig extends ResourceConfig{

	public JerseyConfig() {
		register(JerseyTestController.class);
		register(RequestContextFilter.class);
	}
}
