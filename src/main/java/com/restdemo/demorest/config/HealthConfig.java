package com.restdemo.demorest.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.restdemo.demorest.health.ComponentHealthCheck;

@Configuration
public class HealthConfig {

	@Value("${component.a.health.url}")
	private String componentAHealthUrl;

	/*
	@Bean
	public ComponentHealthCheck componentAHealthCheck() {
		return new ComponentHealthCheck(componentAHealthUrl);
	}*/
}
