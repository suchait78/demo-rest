package com.restdemo.demorest.config;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.restdemo.demorest.consul.ConsulDiscoveryClient;

@Configuration
public class ConsulConfig {

	@Autowired
	private DiscoveryClient discoveryClient;
	
	@Value("${lookup.component.a}")
	private String applicationServiceName;
	
	/*
	@Bean(name = "jmsDiscovery")
	public URI getJmsAppConsulDiscovery() {
		
		ConsulDiscoveryClient consulDiscoveryClient = new ConsulDiscoveryClient(applicationServiceName);
		consulDiscoveryClient.setDiscoveryClient(discoveryClient);
		
		Optional<URI> optionalURI = consulDiscoveryClient.getServiceURL();
		
		if(optionalURI.isPresent()) {
			return optionalURI.get();
		}
		
		return null;	
	}
	*/
}
